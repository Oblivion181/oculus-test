﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightsaber : Grabable {

	public GameObject bladePrefab;
	public float bladeLength;
	public float bladeWidth;
	public Material bladeMaterial;

	Transform blade;
	bool activated = false;
	public bool focus = false;

	bool animating = false;

	Vector3 bladeZero;
	Vector3 bladeExteded;

	Vector3 extededPos;
	Vector3 retractedPos;

	// Use this for initialization
	void Start () {

		blade = Instantiate(bladePrefab, this.transform).transform;

		if (bladeMaterial != null)
		{
			blade.gameObject.GetComponent<MeshRenderer>().material = bladeMaterial;
		}

		bladeZero = new Vector3(bladeWidth, 0, bladeWidth);
		bladeExteded = new Vector3(bladeWidth, bladeLength, bladeWidth);

		extededPos = new Vector3(0, 0, bladeLength);
		retractedPos = Vector3.zero;
	}

	public override void AButton()
	{
		ToggleBlade();
	}

	public void ToggleBlade(float duration = 2)
	{
		if (!animating)
		{
			animating = true;
			StartCoroutine(ToggleBladeEnum(duration));
		}
	}

	IEnumerator ToggleBladeEnum(float duration = 1)
	{
		float time = 0;
		while (time < 1)
		{
			time += Time.deltaTime * duration;
			
			if(time >=1)
			{
				time = 1;
			}

			if (activated)
			{
				blade.localScale = Vector3.Lerp(bladeExteded, bladeZero, time);
				blade.localPosition = Vector3.Lerp(extededPos, retractedPos, time);
			}
			else
			{
				blade.localScale = Vector3.Lerp(bladeZero, bladeExteded, time);
				blade.localPosition = Vector3.Lerp(retractedPos, extededPos, time);
			}
			yield return null;
		}
		activated = !activated;
		animating = false;
		print("Blade finished");
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	float lifeSpan = 20;
	float timer = 0;

	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if(timer >= lifeSpan)
		{
			Destroy(this.gameObject);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabable : MonoBehaviour {
	
	public bool snap = true;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{

	}

	public virtual void Trigger()
	{

	}

	public virtual void AButton()
	{

	}

	public virtual void XButton()
	{

	}
}

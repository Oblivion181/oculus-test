﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : Grabable {

	public bool focus;

	public Transform reciver;
	public Transform firingPort;
	public GameObject bullet;

	Vector3 reciverStartPos;
	public float reciverMoveAmount;
	public float bulletForce = 10;

	bool firing = false;

	// Use this for initialization
	void Start () {
		reciverStartPos = reciver.position;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButton(0))
		{
			Fire();
		}
	}

	public void Fire()
	{
		if (!firing)
		{
			firing = true;
			StartCoroutine(FireEnum());
		}
	}

	IEnumerator FireEnum()
	{
		float time = 0;
		float speed = 15;

		while (time < 1)
		{
			time += Time.deltaTime * speed;

			reciver.position = Vector3.Lerp(reciverStartPos, reciverStartPos + (reciver.forward * reciverMoveAmount * 0.1f), time);
			yield return null;
		}

		GameObject newBullet = Instantiate(bullet, firingPort.position, firingPort.rotation);
		newBullet.GetComponent<Rigidbody>().AddForce(-newBullet.transform.forward * bulletForce);

		time = 0;

		while (time < 1)
		{
			time += Time.deltaTime * speed;

			reciver.position = Vector3.Lerp(reciverStartPos + ( reciver.forward * reciverMoveAmount * 0.1f ), reciverStartPos, time);
			yield return null;
		}

		yield return new WaitForSeconds(0.3f);

		firing = false;
	}
}
